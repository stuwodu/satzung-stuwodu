# StuWoDu e. V. Satzung

(Inhaltsverzeichnis)

**Hinweis:**

Um die Lesbarkeit der Satzung zu verbessern, wird auf eine geschlechter-spezifische Differenzierung
verzichtet. Entsprechende Begriffe gelten im Sinne der Gleichbehandlung für alle Geschlechter.

**Versionshistorie:**

- Erste Fassung beschlossen von der Gründungsversammlung am 20. März 2006
- Letzte bekannte Version von 2016, geändert durch die Mitgliederversammlung
- Geändert durch die Mitgliederversammlung am (Datum der neuen Änderung)

---

## §1 Name, Sitz, Geschäftsjahr, Organe

1. Der Verein trägt den Namen StuWoDu.
2. Er soll in das Vereinsregister eingetragen werden und trägt dann den Zusatz „e.V.“
3. Der Sitz des Vereins ist im Studentenwohnheim Dutzendteichstraße 8, 10 in Nürnberg, nachfolgend Wohnheim genannt.
4. Das Geschäftsjahr ist jeweils vom 01.10. bis zum 30.09. Es wird mit der Jahreszahl desjenigen Jahres bezeichnet, in dem es endet.

## §2 Vereinszweck

Der Verein sieht seine Hauptaufgaben in der Förderung des Zusammenlebens der Bewohner des Studentenwohnheims und der Erweiterung des sozialen Gemeinschaftslebens. Dieser Zweck wird u. a. erreicht durch:

- Durchführung von Veranstaltungen
- Bereitstellung von Gemeinschaftseinrichtungen
- Förderung und Organisation sportlicher Aktivitäten
- Bereitstellung und Sicherung des IT-Netzwerkes

In Folge dessen unterstützt der Verein den sozialen, kulturellen und wissenschaftlichen Austausch, insbesondere auf Hochschulebene in Einklang mit § 52 Abs. 2 Nr. 1 AO. Grundlage seines Handelns sind Toleranz, Hilfsbereitschaft und partnerschaftliche Zusammenarbeit. Als Veranstaltungen sind beispielsweise Informations- und Koordinations- und Gemeinschaftsveranstaltungen zur Verbesserung der studentischen Leistung und des Austauschs oder Integrationsveranstaltungen für ausländische Studenten zu verstehen. Sportliche Aktivitäten finden für die Mitglieder sowohl regelmäßig, zum Beispiel in Form von Fußballtraining oder Tischtennis, als auch einmalig in Form von Turnieren statt. Der StuWoDu e. V. verfolgt keine politischen, religiösen oder gewerblichen Ziele.

## §3 Gemeinnützigkeit

Der Verein verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke und ist selbstlos tätig. Mittel des Vereins dürfen nur für satzungsgemäße Zwecke verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins. Vereinsraumteam, das Netzwerkteam, Verwaltung und Vorstand erhalten eine Aufwandsentschädigung zur Deckung persönlicher Kosten für den Verein. Es
darf keine Person durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

## §4 Datenschutz

1. Zur Erfüllung der Zwecke und Aufgaben des Vereins werden unter Beachtung der Vorgaben der EU-Datenschutz-Grundverordnung (EU-DSGVO) und des Bundesdatenschutzgesetzes (BDSG-neu) personenbezogene Daten der Mitglieder im Verein verarbeitet.
2. Auf der Grundlage der EU-DSGVO gibt sich der Verein eine Datenschutzrichtlinie. Sie
wird der Satzung als Anlage beigefügt.
3. Die Mitgliederversammlung beschließt bei Einführung einmalig über die Datenschutzrichtlinie. Etwaige Änderungen beschließt der Vorstand und informiert die Mitglieder in der folgenden Mitgliederversammlung

## §5 Mitgliedschaft

1. Natürliche und juristische Personen sowie andere Vereinigungen können Mitglieder werden.
2. Der Vorstand entscheidet über die Aufnahme, die in Textform beantragt werden muss. Ein Rechtsanspruch besteht nicht.
3. Die Mitgliedschaft endet durch Austritt, Wohnheim-Auszug (außer Ehrenmitgliedschaft oder Förderstatus), Tod oder Rechtsunfähigkeit.
4. Mitglieder können das Netzwerk nutzen. Bei Nichterfüllung von Pflichten kann der Zugang gesperrt werden.
5. Bei Beendigung der Mitgliedschaft erlöschen alle Ansprüche; keine Rückerstattungen. Rückständige Beiträge bleiben fällig.
6. Mitgliedsarten und Beiträge sind in einer separaten Ordnung festgelegt, die von der Mitgliederversammlung eingeführt und vom Ausschuss geändert wird. Änderungen werden in der nächsten Mitgliederversammlung bekannt gegeben.
7. Ehrenmitglieder zahlen keine Beiträge.

## §6 Organe

Der Verein besitzt folgende Organe:

1. Mitgliederversammlung
2. Stockwerksversammlungen
3. Gremium
4. Ausschuss
5. Vorstand
6. Verwaltung
7. Netzwerkteam

Der Heimleiter hat in beratender Funktion einen Sitz in allen Organen.

Jedes stimmberechtigte Mitglied in einem Organ kann eine Vertretung ernennen, die noch kein stimmberechtigtes Mitglied ist und seine Stimme der Vertretung übertragen. Die Schriftführung oder eine Vertretung hat in keinem Fall Stimmrecht.

### §6.1 Mitgliederversammlung

Teilnehmer der Mitgliederversammlung (MV) sind alle ordentlichen Mitglieder. Die MV findet in den ersten zwei Monaten des Semesters statt. Orientiert wird sich am spätesten Semesterbeginn der TH oder der FAU. Die MV wird durch den Vorstand einberufen und muss mindestens eine Woche vorher durch Aushang in beiden Häusern bekannt gegeben werden. Die Tagesordnung setzt der Vorstand fest.

Aufgaben der MV sind:

1. Bestimmung des Protokollführers.
2. Abgabe der Berichte durch Vorstand und Verwaltung.
3. Entlastung des Vorstands und der Verwaltung.
4. Wahl des Vorstands (§xy) und der Verwaltung (ohne Netzwerkleiter).
5. Satzungsänderungen
6. Auflösung des Vereins (Gem. §xy)

Die MV wird vom 1. Vorsitzenden, bei dessen Verhinderung vom 2. Vorsitzenden oder einem anderen Vorstandsmitglied geleitet. Ist kein Vorstandsmitglied anwesend, bestimmt die MV den Leiter.

Die MV ist ohne Rücksicht auf die Zahl der erschienenen Vereinsmitglieder beschlussfähig. In der Mitgliederversammlung hat jedes Mitglied eine Stimme. Bei Abstimmung entscheidet die einfache Mehrheit.

### §6.2 Stockwerksversammlungen

Teilnehmer der Stockwerksversammlung (SV) sind die Bewohner des jeweiligen Stockwerkes des Wohnheims. Die SV findet je Stockwerk mindestens einmal pro Semester genannt, statt.
WGs werden wie ein eigenes Stockwerk behandelt. Einzelzimmerapartments sind von diesem Organ ausgenommen.

Die SV bestimmt durch einfache Mehrheit jedes Semester eine Stockwerksvertretung.

Die Stockwerksvertreter vertreten das jeweilige Stockwerk im Gremium.

### §6.3 Gremium

Das Gremium besteht aus den:

1. Stockwerksvertretern
2. Vorstand
3. Verwaltung

Jeder Stockwerksvertreter, Vorsitzende oder Verwaltungsmitglied hat je eine Stimme. Für die Entscheidung werden die Stimmen wie folgt gewichtet:

- Stockwerksvertreter: 51%
- Vorstand: 24%
- Verwaltung: 25%

Die Gewichtung richtet sich nach der Anzahl der Stimmberechtigen je Bereich, nicht Anwesende eingerechnet.
Die Schriftführung hat kein Stimmrecht.

Das Gremium tritt mindestens einmal im Semester durch Einberufung des Vorstands zusammen. Der Vorstand ist verpflichtet eine Gremiums-Versammlung (GV) einzuberufen wenn 25% der Gremiumsmitglieder dies unter Angabe des Zwecks und der Gründe schriftlich verlangt. Die Einladung der Gremiumsmitglieder muss in jedem Fall mindestens eine Woche vorher erfolgen.

Das Gremium ist beschlussfähig ab Anwesenheit von 25% seiner Mitglieder. Ist dies nicht erfüllt, wird die GV um mindestens 24h vertagt und ist unabhängig von der Anwesenheitsanzahl beschlussfähig.
Beschlüsse werden mit einfacher Mehrheit entschieden. Über die Beschlüsse muss Protokoll geführt werden.

### §6.4 Vorstand

1. Der Vorstand besteht aus
   - dem 1. Vorsitzenden
   - dem 2. Vorsitzenden
   - dem Buchhaltungsleiter
2. Der Verein wird nach außen vertreten durch alle Mitglieder des Vorstands. Diese sind jeweils einzeln vertretungsberechtigt.
3. Jedes ordentliche Mitglied kann Vorstandsmitglied werden. Der Vorstand wird von der MV für die Dauer eines Semesters gewählt. Die Amtsperiode des Vorstands endet mit der Entlastung des Vorstands, sowie des Buchhaltungsleiters durch die MV. Scheidet ein Mitglied des Vorstands vor Ablauf der Amtsdauer aus, so wird für den Rest der Amtszeit vom Gremium ein neues Mitglied gewählt.
4. Beschlüsse fasst der Vorstand mit einfacher Mehrheit. Über Beschlüsse sind der Ausschuss und das Gremium zu informieren und ist Protokoll zu führen.
5. Der Vorstand erhält volle Buchhaltungs- und Bankvollmachten.
6. Der Buchhaltungsleiter hat monatlich die Bücher zu führen und je Semester die Finanzberichte zu erstellen. Zudem zeichnet er sich für die operative Buchführung des Vereins verantwortlich.
7. Für Ausgaben ist ein Beschluss zu fassen. Für Ausgaben die über 5% der zuletzt eingenommen Beiträge liegen ist ein Beschluss des Ausschusses nötig.

### §6.5 Ausschuss

1. Der Ausschuss besteht aus
   - Vorstand
   - Verwaltung
2. Ziel des Ausschusses ist es Entscheidungen, über die das Gremium entscheiden muss, vorzubereiten.
3. Ausschusssitzungen werden mindestens 24 Stunden vorher angekündigt.
4. Der Ausschuss ist beschlussfähig mit 50% Anwesenheit. Wird dies nicht erreicht, wird um mindestens 24h vertagt und eine Beschlussfähigkeit ist dann unabhängig der Teilnehmerzahl gegeben.
5. Beschlüsse über Ausgaben, die 50% der Beitragseinnahmen des aktuellen Semesters übersteigen, müssen im Gremium beschlossen werden.

## §6.6 Verwaltung

1. Die Verwaltung besteht mindestens aus den folgenden Bereichsleitern
    - Netzwerkleitung
    - Vereinsraumleitung
    - Sportleitung
    - Schriftführung
2. Es können weitere Bereiche geschaffen werden. Bereichsleiter sind mit Ausnahme der Netzwerkleitung durch die MV zu wählen. Weitere Bereichsleiter können zum Beispiel sein: Parkplatzleitung
3. Jeder Bereichsleiter kann in Eigenverantwortung ein Team bilden, das ihn in seiner Tätigkeit unterstützt. Die Teammitglieder nehmen bei Bedarf beratend an Gremium- oder Ausschusssitzungen teil und erhalten jedoch keine eigenen Stimmrechte. Die Bereichsleiter können Stellvertreter ernennen und diese in die entsprechenden Organe entsenden.

## §7 Netzwerkteam

1. Das Netzwerkteam betreut und verwaltet die Infrastruktur und den Datenverkehr im Studentenwohnheim im Auftrag des Regionalen Rechenzentrums Erlangen (RRZE), gemäß der Richtlinien für die Nutzung des FAU-Datennetzes, sowie der Benutzungsrichtlinien für Informationsverarbeitungssysteme der Universität Erlangen-Nürnberg.
2. Die Auswahl des Netzwerkleiters erfolgt auf Vorschlag des Netzwerkteams zu Beginn jedes Semesters. Die Heimleitung ernennt die Netzwerkleitung.
3. Der Netzwerkleiter vertritt in den entsprechenden Vereinsorganen die Interessen des Netzwerkteams.
4. Zur Sicherstellung des reibungslosen Netzwerkbetriebes erhält das Netzwerkteam ein unabhängiges Buchungskonto für finanzielle Rücklagen. Die Buchhaltungs- und Bankvollmachten darüber liegen beim Vorstand (§xy). Bei Anschaffungen die einen Betrag über 1.000,- € betreffen, muss eine Freigabe durch den Vorstand eingeholt werden, dieser beruft hierzu, sofern angemessen eine Ausschuss- oder Gremiumssitzung ein.
5. Die finanziellen Mittel des Netzwerksbudget werden durch die Beitragsordnung festgelegt.
6. Überschüsse des Netzwerks werden auf das Buchungskonto für finanzielle Rücklagen gebucht.

## §8 Schriftführer

1. Die Aufgabe des Schriftführers besteht darin, die Entscheidungen und Beschlüsse der MV(§xy), des Ausschusses (§xy) und des Gremiums (§xy) schriftlich festzuhalten und nach den Versammlungen entsprechend den jeweiligen Teilnehmern zur Verfügung zu stellen.
2. Des Weiteren übernimmt er die Aufgabe des Kassenprüfers, er überprüft regelmäßig spätestens jedoch am Ende eines Semesters die Rechtmäßigkeit der vorhandenen Kassenbücher und zum Anfang des Semesters den Gesamt-Finanzbericht über das Vorsemester.
3. Der Schriftführer erstellt die Aufgabenbereiche des Vorstands und der Verwaltung (ohne Netzwerkleiter), welche im Ausschuss abgesegnet werden.

## §9 Fördertopf

1. Der Fördertopf wird in einer separaten Ordnung geregelt.
2. Die erste Fassung der Fördertopfordnung bestimmt die MV.
3. Änderungen der Fördertopfordnung beschließt das Gremium am Anfang des Semesters.

## §10 Vereinsauflösung

1. Für die Auflösung des Vereins ist eine Zwei-Drittel-Mehrheit der in der MV anwesenden stimmberechtigten Mitglieder erforderlich. Die Tagesordnung muss die Auflösung ausdrücklich als Beratungsgegenstand beinhalten.
2. Bei Auflösung oder Aufhebung des Vereins oder bei Wegfall der steuerbegünstigter Zwecke fällt das Vermögen des Vereins dem Studierendenwerk Erlangen Nürnberg zu, der es unmittelbar und ausschließlich für  gemeinnützige Zwecke im Sinne von §2 zu verwenden hat.

## §11 Schlussbestimmungen

1. Salvatorische Klausel
    Sollten einzelne Bestimmungen dieser Satzung unwirksam sein oder werden, so wird dadurch die Wirksamkeit der übrigen Bestimmungen nicht berührt.
2. Sonstiges
    Online-Sitzungen und Abstimmungen sind zulässig. Regelungen zum Netzwerkzugang und möglichen Sanktionen sind in der Nutzungsordnung für das Netzwerk festgelegt.
3. Gültigkeit der Satzung
    Alte Satzungen gelten bis zum Gültigwerden einer neuen Satzung. Eine neue Satzung tritt mit ihrer Eintragung ins Vereinsregister in Kraft.
4. Der Vorstand wird beauftragt, die Satzung beim Vereinsregister eintragen zu lassen.
5. Sollten bei der Eintragung ins Vereinsregister geringfügige redaktionelle und/oder inhaltliche Änderungen erforderlich werden, ist der Vorstand ermächtigt, diese Änderungen ohne Einberufung einer MV vorzunehmen. Diese Ermächtigung des Vorstands zur Abänderung der Satzung gilt in gleicher Weise, wenn von Seiten des Finanzamts redaktionelle und/oder inhaltliche Änderungen erforderlich werden sollten. Die MV ist über diese Änderungen bei der nächsten Versammlung zu informieren.
